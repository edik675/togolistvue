<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Place;

class PlacesController extends Controller
{
    public function add(Request $request)
    {
        $result = new \stdClass();

        $data = $request->all();

        $id_place = isset($data['id']) && $data['id'] ? $data['id'] : false;
        if($id_place) {
            $place = Place::find($id_place);
        }
        else {
            $place = new Place;
//            $place->user_id = auth()->id();
            $place->user_id = isset($data['id_user']) ? $data['id_user'] : false;
        }

        if(isset($data['lat'])) {
            $place->latitude = $data['lat'];
        }

        if(isset($data['lng'])) {
            $place->longitude =  $data['lng'];
        }

        if(isset($data['name'])) {
            $place->name = $data['name'];
        }

        if(isset($data['visited'])) {
            $place->visited = $data['visited'];
        }

        $res = $place->save();
        if($res) {
            $result->success = 1;
            $result->place_info = Place::find($place->id);

            if(!$id_place) {
                $result->html = (string) view('places.list_item', ['item' => $result->place_info]);
            }
        }

        return response()->json($result);
    }

    public function get(Request $request) {
        // ToDo here add auth check, or something like that - no "auth()->id()"


        $data = $request->all();
        $id_user = isset($data['id_user']) ? $data['id_user'] : false;
        if(!$id_user) {
            return '';
        }


        $user = User::find($id_user);
        $places = [];
        if($user) {
            if($user->places) {
                foreach ($user->places as $item) {
                    $places[$item->id] = $item;
                }
            }
        }
        return $places ? $places : '';
    }

    public function delete(Request $request) {
        $result = new \stdClass();

        $data = $request->all();
        $id = $data['id'];

        $place = Place::find($id);
        if(!$place) {
            $result->success = 0;
        }

        $res = $place->delete();
        $result->success = $res ? 1 : 0;
        return response()->json($result);
    }

    public function visited(Request $request) {
        $result = new \stdClass();

        $data = $request->all();
        $id = $data['id'];
        $visited = $data['visited'];

        $place = Place::find($id);
        $place->visited = $visited;
        $place->save();

        $result->success = 1;
        return response()->json($result);
    }
}
