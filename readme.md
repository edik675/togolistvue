## ToGo-list

ToGo list by using Google Maps

This is a project for people who like travel.
Here you can organize your trip: adding places on map and keep it in our database.
Also you can edit it, set us "visited".

#### I used

- Laravel Framework 5.5.32
- PHP 7.1
- Vue.js 
- MySql

#### I do not used

- SCSS

# Task

ToGo-list

Make a ToGo list by using Google Maps, and let the user save places the user wants to visit.

Features:
1. User clicks on a place on the map, a marker is created and a popup appears on that place with some input field to name the place and an option to save to the ToGo list. Saved places are listed somewhere on the view.
2. User can later choose a place from the list and see it on the map
3. User can filter saved places
4. User can remove or mark places visited
5. As we discussed use backend, for having accounts and storing locations for user

Our development team uses:
* VueJS with ES2016+ (Babel).
* SCSS
* Laravel

Comment code, use git repo and commit often.
Please also write a README.md to describe the project and your thoughts on how you solved it.

Send in the assignment including the git repo.

#Help tutorials
* https://codeburst.io/api-authentication-in-laravel-vue-spa-using-jwt-auth-d8251b3632e0
* https://www.npmjs.com/package/vue2-google-maps