<div class="list-group">
    <?php
    if(isset($places) && $places) {
        foreach ($places as $item) {
            echo (string) view('places.list_item', ['item' => $item]);
        }
    }
    ?>
</div>